for i in $(seq 40 159);
do
  python3 ../../preprocess/createdata.py --filename AC_training_$i_1.seq --neg_type dinucleotide --outputprefix TF_training_$i_1
  python3 ../../preprocess/createdata.py --filename B_testing_$i_1.seq --outputprefix TF_testing_$i_1 --reverse False
  python3 ../../ezgeno/ezgeno.py --trainFileList TF_training_$i_1.sequence --trainLabel TF_training_$i_1.label --testFileList TF_testing_$i_1.sequence --testLabel TF_testing_$i_1.label --layers 3 --cuda 0 --save example_$i_1.model
  python3 ../../ezgeno/visualize.py --show_seq all --load example_$i_1.model --data_path ./visualization_$i_1.fa --data_name TF --target_layer_names "[2]" --use_cuda True
done