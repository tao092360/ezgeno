import argparse

def extract_peak(input_path, output_path):
    output = open(output_path, 'w')
    with open(input_path, 'r') as file_in:
        f = file_in.read().splitlines()
        for line in f:
            line = line.strip()
            data = line.split()
            output.write(data[-1])
            output.write('\n')

def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", type=str,
                        default='./input_3.txt', help="Input file root.")
    parser.add_argument("--output", type=str,
                        default='./output_3.txt', help="Output file root.")
    return parser


if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()

    extract_peak(args.input, args.output)
