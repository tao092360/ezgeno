#!/bin/bash
# Program:
#       data processing
# History:
# 2022/05/06	TAOYU	First release
NoTopPeak=1755

for i in $(seq 60 64)
do
	sed -e 's/^chr//' ./step1/narrowpeaks_${i}.bed > ./step1/narrowpeaks_rmchr_${i}.bed
	sort -r -k7 -n ./step1/narrowpeaks_rmchr_${i}.bed| tail -n $NoTopPeak > ./step2/reverse_1750/step2_input_${i}.bed
	bedtools getfasta -bed ./step2/reverse_1750/step2_input_${i}.bed -fi ./reference/Homo_sapiens.GRCh38.dna.primary_assembly.fa > ./step3/reverse_1750/step3_input_${i}.fa
	wc -l ./step1/narrowpeaks_rmchr_${i}.bed
done

for i in $(seq 60 64)
do
	python peak_location.py --input ./step2/reverse_1750/step2_input_${i}.bed --output ./step2/reverse_1750/peak_site_${i}.txt

done

for i in $(seq 60 64)
do
	python train_test_data.py --input_fasta ./step3/reverse_1750/step3_input_${i}.fa --input_peak_center ./step2/reverse_1750/peak_site_${i}.txt --output_train ./step4/reverse_1750/AC_training_${i}.seq --output_test ./step4/reverse_1750/B_testing_${i}.seq --output_visualization ./step4/reverse_1750/visualization_${i}.fa --data_size 1750

done
