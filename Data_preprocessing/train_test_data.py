import argparse

def fasta_peaksite_merged(fasta_path, peak_center_path, train_path, test_path, visualization_path, data_size):
    peak_center = open(peak_center_path, "r")
    train = open(train_path, 'w')
    #test = open(test_path, 'w')
    visualization = open(visualization_path, 'w')

    train.write('FoldID' + '\t' + 'EventID seq' + '\t' + 'Bound' + '\t' + '' +'\n')
    #test.write('FoldID' + '\t' + 'EventID seq' + '\t' + 'Bound' + '\t' + '' +'\n')
    
    peak = dict()
    for i in peak_center:
        peak[len(peak)] = i

    with open(fasta_path, 'r') as file_in:
        f = file_in.read().splitlines()
        num = 0
        for line in f:
            line = line.strip()
            if len(line) < 101:
                continue

            center = int(peak[num])
            start, end = center-50, center+51

            if start <= 0:
                start, end = 0, end + abs(start)
            elif end >= len(line):
                start, end = start - abs(end - len(line)), len(line)

            data = line[start:end]
            if len(data) < 101:
                continue

            if (num+1) < data_size:
                num+=1
                train.write('A' + '\t' + 'seq_%05d_peak'%num + '\t' + data + '\t' + '1' +'\n')
                
                '''
                if num % 2 == 1 or num > 500:
                    train.write('A' + '\t' + 'seq_%05d_peak'%num + '\t' + data + '\t' + '1' +'\n')
                else:
                    test.write('A' + '\t' + 'seq_%05d_peak'%num + '\t' + data + '\t' + '1' +'\n')
                    visualization.write(data + '\n')
                '''


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_fasta", type=str,
                        default='./input_3.txt', help="Input fasta file root.")
    parser.add_argument("--input_peak_center", type=str,
                        default='./input_3.txt', help="Input peak file root.")
    parser.add_argument("--output_train", type=str,
                        default='./output_3.txt', help="Output train set file root.")
    parser.add_argument("--output_test", type=str,
                        default='./output_3.txt', help="Output test set file root.")
    parser.add_argument("--output_visualization", type=str,
                        default='./output_3.txt', help="Output visualization file root.")
    parser.add_argument("--data_size", type=int,
                        default=2000, help="Data size you need.")
    return parser


if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()

    fasta_peaksite_merged(args.input_fasta, args.input_peak_center, args.output_train, args.output_test, args.output_visualization, args.data_size)
