import argparse

def create_bed(fasta, sequence):
    output_file = open(sequence, 'w')

    with open(fasta, 'r') as file:
        f = file.read().splitlines()

        for line in f:
            line = line.split('\t')
            
            if line[0].startswith('>'):
                continue

            output_file.write(line[0] + '\n')


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_fa", type=str,
                        default='./Liver_neg_read.fa', help="Input fasta file root.")
    parser.add_argument("--output_seq", type=str,
                        default='./Liver_neg_read_ref.sequence', help="output read file root.")

    return parser

if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()

    create_bed(args.input_fa, args.output_seq)