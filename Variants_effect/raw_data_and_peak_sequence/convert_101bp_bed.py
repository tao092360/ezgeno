import argparse

def create_bed(vcf_path, bed_path):
    bed_file = open(bed_path, 'w')

    with open(vcf_path, 'r') as file:
        f = file.read().splitlines()

        for line in f:
            line = line.split('\t')
            
            if line[0].startswith('chr'):
                start = int(line[1]) - 51
                end = int(line[1]) + 50
                bed_file.write(line[0] + '\t' + str(start) + '\t' + str(end) + '\n')


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_vcf", type=str,
                        default='./Breast_Mammary_Tissue_neg.vcf', help="Input vcf file root.")
    parser.add_argument("--output_bed", type=str,
                        default='./Breast_neg_read.bed', help="output read of bed file root.")

    return parser

if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()

    create_bed(args.input_vcf, args.output_bed)
