import argparse

def create_bed(fasta, vcf, sequence):
    output_file = open(sequence, 'w')
    ref_alt = dict()
    n=0
    with open(vcf, 'r') as file:
        f = file.read().splitlines()

        for line in f:
            line = line.split('\t')
            
            if line[0].startswith('chr'):
                element = line[2].split('_')
                ref_alt[n] = [element[2], element[3]]
                n+=1

    num = 0
    with open(fasta, 'r') as file:
        f = file.read().splitlines()

        for line in f:
            if line[50] == ref_alt[num][0]:
                seq = line[:50] + ref_alt[num][1] + line[51:]
                output_file.write(seq + '\n')
            else:
                print(num)
            num+=1
            


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_fa", type=str,
                        default='./Liver_neg_read_ref.sequence', help="Input ref read file root.")
    parser.add_argument("--input_vcf", type=str,
                        default='./Liver_neg.vcf', help="Input vcf file root.")
    parser.add_argument("--output_fa", type=str,
                        default='./Liver_neg_read_alt.sequence', help="output alt read file root.")

    return parser

if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()

    create_bed(args.input_fa, args.input_vcf, args.output_fa)