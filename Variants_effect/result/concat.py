import csv
import numpy as np
from scipy import stats

# 結合相同組織的24種轉錄因子結合預測結果
# 包含 pos_ref pos_alt neg_ref neg_alt 四種
# 計算個別轉錄因子結合預測，具有顯著性的數量 --> 附錄C

variant_list = ["Breast", "Liver", "Lung"]
seq_num_list = [1129, 352, 1653]
p_num_in_TF = [["TF","RAD21","MYC","ELK1","POLR2A","MAFK","CREB1","SIN3A","YY1","JUND","TAF1","TCF12","EP300","SP1","CEBPB","MAX","RCOR1","ELF1","ZBTB33","HDAC2","RFX5","GABPA","ESRRA","SREBF1","ZNF24"], 
                ["p-value<0.05"], ["p-value<0.01"], ["p-value<0.005"], ["p-value<0.05"], ["p-value<0.01"], ["p-value<0.005"]]


for index in variant_list:
    variant = variant_list[index]
    seq_num = seq_num_list[index]

    path = './{}_score/'.format(variant)
    total, pos_p_num, neg_p_num = 0, 0, 0
    for target in range(1,25):

        data_dict = dict()
        if variant == "Lung" and target == 24:
            p_num_in_TF[0].remove("ZNF24")
            continue
        elif variant == "Breast" and target == 8:
            p_num_in_TF[0].remove("YY1")
            continue

        for num in range(10):
            # pos ref
            filename = path + 'pos_ref_score_{}_{}.csv'.format(target, num+1)
            f = open(filename, 'r')
            n = 1
            
            label="pos_ref_{}".format(num+1)
            if 0 not in data_dict:
                data_dict[0] = [label]
            else:
                data_dict[0].append(label)

            for i in f:
                i = float(i.strip())
                if n > seq_num:
                    break

                if n not in data_dict:
                    data_dict[n]=[i]
                else:
                    data_dict[n].append(i)
                n += 1

        for num in range(10):

            # pos alt
            filename = path + 'pos_alt_score_{}_{}.csv'.format(target, num+1)
            f = open(filename, 'r')
            n = 1
            label="pos_alt_{}".format(num+1)
            data_dict[0].append(label)
            for i in f:
                i = float(i.strip())
                if n > seq_num:
                    break

                data_dict[n].append(i)
                n +=1

        for num in range(10):

            # neg ref
            filename = path + 'neg_ref_score_{}_{}.csv'.format(target, num+1)
            f = open(filename, 'r')
            n = 1
            label="neg_ref_{}".format(num+1)
            data_dict[0].append(label)
            for i in f:
                i = float(i.strip())
                if n > seq_num:
                    break

                data_dict[n].append(i)
                n += 1

        for num in range(10):

            # neg alt
            filename = path + 'neg_alt_score_{}_{}.csv'.format(target, num+1)
            f = open(filename, 'r')
            n = 1
            label="neg_alt_{}".format(num+1)
            data_dict[0].append(label)
            for i in f:
                i = float(i.strip())
                if n > seq_num:
                    break

                data_dict[n].append(i)
                n += 1

        data_dict[0].append("pos_ttest")
        data_dict[0].append("neg_ttest")
        data_dict[0].append("pos_p")
        data_dict[0].append("neg_p")


        pos_p_num_in_TF_1, pos_p_num_in_TF_2, pos_p_num_in_TF_3 = 0, 0, 0
        neg_p_num_in_TF_1, neg_p_num_in_TF_2, neg_p_num_in_TF_3 = 0, 0, 0
        p_value_threshold = 0.05
        for i in range(seq_num):
            _, pos_ttest = stats.ttest_rel(data_dict[i+1][:10], data_dict[i+1][10:20])
            _, neg_ttest = stats.ttest_rel(data_dict[i+1][20:30], data_dict[i+1][30:40])
            
            if pos_ttest < 0.05:
                pos_p_num_in_TF_1 += 1
                pos_p = pos_ttest
                pos_p_num += 1
            else:
                pos_p = 1

            if neg_ttest < 0.05:
                neg_p_num_in_TF_1 += 1
                neg_p = neg_ttest
                neg_p_num += 1
            else:
                neg_p = 1

            if pos_ttest < 0.01:
                pos_p_num_in_TF_2 += 1
                pos_p = pos_ttest
                pos_p_num += 1
            else:
                pos_p = 1

            if neg_ttest < 0.01:
                neg_p_num_in_TF_2 += 1
                neg_p = neg_ttest
                neg_p_num += 1
            else:
                neg_p = 1
            
            if pos_ttest < 0.005:
                pos_p_num_in_TF_3 += 1
                pos_p = pos_ttest
                pos_p_num += 1
            else:
                pos_p = 1

            if neg_ttest < 0.005:
                neg_p_num_in_TF_3 += 1
                neg_p = neg_ttest
                neg_p_num += 1
            else:
                neg_p = 1

            total += 1
            
            
            data_dict[i+1].append(pos_ttest)
            data_dict[i+1].append(neg_ttest)
            data_dict[i+1].append(pos_p)
            data_dict[i+1].append(neg_p)
        
        p_num_in_TF[1].append(pos_p_num_in_TF_1)
        p_num_in_TF[2].append(pos_p_num_in_TF_2)
        p_num_in_TF[3].append(pos_p_num_in_TF_3)
        p_num_in_TF[4].append(neg_p_num_in_TF_1)
        p_num_in_TF[5].append(neg_p_num_in_TF_2)
        p_num_in_TF[6].append(neg_p_num_in_TF_3)
            
        
        f1 = open(path + '{}_result_{}.csv'.format(variant,target), 'w', newline='')
        writer = csv.writer(f1)
        for i in range(seq_num+1):
            writer.writerow(data_dict[i])

f1 = open(path + '{}_variant_effect_seqNum.csv'.format(variant), 'w', newline='')

for i in range(len(p_num_in_TF[0])):
    for j in range(len(p_num_in_TF)):
        f1.write(str(p_num_in_TF[j][i]))
        if j == len(p_num_in_TF)-1:
            f1.write('\n')
        else:
            f1.write(',')
# print(total, pos_p_num, neg_p_num)
