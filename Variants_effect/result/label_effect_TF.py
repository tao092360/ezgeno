target = "Lung"
TF_num = {"Liver":24, "Breast":23, "Lung":23}

f = open("./{}_score/{}_variant_effect_seqNum.csv".format(target, target), "r")
TF_list = []
p_05 = []
p_01 = []
p_005 = []
total_list = []
for line in f:
    if line.startswith("TF"):
        continue

    line = line.strip()
    element = line.split(",")

    if element[0] == "Total":
        pass
    TF_list.append(element[0])
    p_05.append(element[1])
    p_05.append(element[4])
    p_01.append(element[2])
    p_01.append(element[5])
    p_005.append(element[3])
    p_005.append(element[6])


    print(element[1])
    print(p_05)

