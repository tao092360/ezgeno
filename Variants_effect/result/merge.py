import csv

# 將顯著性 p-value < threshold 篩選出來 --> 變異位點之結合影響具有顯著性
# Lung 忽略第24個; Breast忽略第8個
# 計算具有顯著性的數量 --> 表3-6

variant_list = ["Breast", "Liver", "Lung"]
seq_num_list = [1129, 352, 1653]
t_test_pos, t_test_neg, p_threshold_pos, p_threshold_neg = dict(), dict(), dict(), dict()


for index in variant_list:
    variant = variant_list[index]
    seq_num = seq_num_list[index]

    pos_p_num, neg_p_num, total = 0, 0, 0
    for target in range(1, 25):

        if variant == "Lung" and target == 24:
            continue
        elif variant == "Breast" and target == 8:
            continue

        f = open('./{}_score/{}_result_{}.csv'.format(variant, variant, target), 'r')
        num = 1
        for i in f:

            i = i.strip()
            element = i.split(',')
            # POS T test[40], NEG T test[41], POS(p<0.05)[42], NEG(p<0.05)[43]
            if 'pos' in element[0]:
                continue

            if num not in t_test_pos:
                t_test_pos[num] = []
                t_test_neg[num] = []
                p_threshold_pos[num] = []
                p_threshold_neg[num] = []

                t_test_pos[num].append(element[-4])
                t_test_neg[num].append(element[-3])
                p_threshold_pos[num].append(element[-2])
                p_threshold_neg[num].append(element[-1])
            else:
                t_test_pos[num].append(element[-4])
                t_test_neg[num].append(element[-3])
                p_threshold_pos[num].append(element[-2])
                p_threshold_neg[num].append(element[-1])

            
            if float(element[-2]) < 0.005:
                pos_p_num += 1

            if float(element[-1]) < 0.005:
                neg_p_num += 1

            num+=1
            total+=1

    print(total, pos_p_num, neg_p_num)

    f1 = open('./{}_pos_t_test_merge.csv'.format(variant), 'w', newline='')
    f2 = open('./{}_neg_t_test_merge.csv'.format(variant), 'w', newline='')
    f3 = open('./{}_pos_p_threshold_merge.csv'.format(variant), 'w', newline='')
    f4 = open('./{}_neg_p_threshold_merge.csv'.format(variant), 'w', newline='')

    writer1 = csv.writer(f1)
    writer2 = csv.writer(f2)
    writer3 = csv.writer(f3)
    writer4 = csv.writer(f4)

    for i in range(1, seq_num):
        writer1.writerow(t_test_pos[i])
        writer2.writerow(t_test_neg[i])
        writer3.writerow(p_threshold_pos[i])
        writer4.writerow(p_threshold_neg[i])
