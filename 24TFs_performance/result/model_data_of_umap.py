import random
import numpy as np

TF_list = ["RAD21","MYC","ELK1","POLR2A","MAFK","CREB1","SIN3A","YY1","JUND","TAF1","TCF12","EP300","SP1","CEBPB","MAX","RCOR1","ELF1","ZBTB33","HDAC2","RFX5","GABPA","ESRRA","SREBF1","ZNF24"]
cell_line_set = ["HepG2", "K562", "GM12878", "MCF-7", "A549"]
cell_line_list = [[] for i in range(len(TF_list))]

f = open('./model_structure/TF_list.txt', 'r')
for line in f:
    line = line.strip()
    TF, cell_line = line.split(',')[0], line.split(',')[1]
    index = TF_list.index(TF)
    cell_line_list[index].append(cell_line)

# 轉錄因子、細胞株 對照表
layer_dict = {"conv_3":3, "dilated_conv_3":5, "conv_7":7, "dilated_conv_7":9, "conv_11":11, "dilated_conv_11":13, "conv_15":15, "dilated_conv_15":17, "conv_19":19, "dilated_conv_19":21}


def model_single_group(target_TF):
    w = open('./model_structure/model_single_{}_umap.csv'.format(target_TF), 'w')
    w.write('TF,cell_line,Layer_1,Layer_2,Layer_3\n')

    target_index = TF_list.index(target_TF)*5 + 40
    for i in range(target_index, target_index+5):
        TF, cell_line = TF_list[(i-40)//5], cell_line_list[(i-40)//5][np.mod((i-40), 5)]
        for j in range(1, 11):
            f = open('model_{}_{}.txt'.format(i, j), 'r')
            w.write('{},{},'.format(TF, cell_line))
            n = 1
            for line in f:

                layer = line.split('\n')[0]
                element = ''
                if 'dilation' in layer:
                    element += 'dilated_conv_'
                elif 'conv' in layer:
                    element += 'conv_'

                if '3' in layer:
                    element += '3'
                elif '7' in layer:
                    element += '7'
                elif '11' in layer:
                    element += '11'
                elif '15' in layer:
                    element += '15'
                elif '19' in layer:
                    element += '19'

                if element != '':
                    layer_size = layer_dict[element]*5
                    w.write(str(layer_size))

                    if n < 3:
                        w.write(',')
                    n += 1

            w.write('\n')

def model_multiple_group(target_TF_list, target_cell_type):
    w = open('./model_structure/same_cell_type_umap_1.csv', 'w')
    w.write('TF,cell_line,Layer_1,Layer_2,Layer_3\n')

    for target_TF in target_TF_list:
        target_index = TF_list.index(target_TF)*5 + 40
        for i in range(target_index, target_index+5):
            TF, cell_line = TF_list[(i-40)//5], cell_line_list[(i-40)//5][np.mod((i-40), 5)]

            if cell_line != target_cell_type:
                continue

            for j in range(1, 11):
                f = open('model_{}_{}.txt'.format(i, j), 'r')
                w.write('{},{},'.format(TF, cell_line))
                n = 1
                for line in f:

                    layer = line.split('\n')[0]
                    element = ''
                    if 'dilation' in layer:
                        element += 'dilated_conv_'
                    elif 'conv' in layer:
                        element += 'conv_'

                    if '3' in layer:
                        element += '3'
                    elif '7' in layer:
                        element += '7'
                    elif '11' in layer:
                        element += '11'
                    elif '15' in layer:
                        element += '15'
                    elif '19' in layer:
                        element += '19'

                    if element != '':
                        layer_size = layer_dict[element]*3+100
                        w.write(str(layer_size))

                        if n < 3:
                            w.write(',')
                        n += 1

                w.write('\n')
'''
for i in range(len(TF_list)):
    model_single_group(target_TF=TF_list[i])
'''
random_TF_list = np.random.choice(TF_list, 6, False)
random_cell_line = np.random.choice(cell_line_set)
print(random_TF_list, random_cell_line)
model_multiple_group(random_TF_list, random_cell_line)
