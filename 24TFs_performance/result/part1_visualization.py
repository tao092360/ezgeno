import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# SIN3A in 5 cell lines.
df = pd.read_excel("prediction.xlsx", usecols=["cell line", "AVG", "STDEV"], nrows=26, sheet_name="SIN3A")

cell_line, top_2000, top_4000, top_6000, top_8000, top_10000 = [], [], [], [], [], []
sd_2000, sd_4000, sd_6000, sd_8000, sd_10000 = [], [], [], [], []
for i in range(len(df["cell line"])):
    if len(cell_line) == 0:
        cell_line.append(df["cell line"][i])
    elif cell_line[-1] != df["cell line"][i]:
        cell_line.append(df["cell line"][i])

    if i % 5 == 0:
        top_2000.append(df["AVG"][i])
        sd_2000.append(df["STDEV"][i])
    elif i % 5 == 1:
        top_4000.append(df["AVG"][i])
        sd_4000.append(df["STDEV"][i])
    elif i % 5 == 2:
        top_6000.append(df["AVG"][i])
        sd_6000.append(df["STDEV"][i])
    elif i % 5 == 3:
        top_8000.append(df["AVG"][i])
        sd_8000.append(df["STDEV"][i])
    else:
        top_10000.append(df["AVG"][i])
        sd_10000.append(df["STDEV"][i])

x = np.arange(len(cell_line))
width = 0.1
plt.figure(figsize=(15,4))
plt.bar(x, top_2000, width, yerr=sd_2000, capsize=3, color='red', label='2000(7:1)')
plt.bar(x+width, top_4000, width, yerr=sd_4000, capsize=3, color='yellow', label='4000(15:1)')
plt.bar(x + 2*width,top_6000, width, yerr=sd_6000, capsize=3, color='orange', label='6000(23:1)')
plt.bar(x + 3*width,top_8000, width, yerr=sd_8000, capsize=3, color='green', label='8000(31:1)')
plt.bar(x + 4*width,top_10000, width, yerr=sd_10000, capsize=3, color='blue', label='10000(39:1)')
plt.xticks(x + 4*width / 2, cell_line)
plt.ylim(0.6, 1)
plt.xlabel('Cell line')
plt.ylabel('AUC score')
plt.title('Average of 10 times AUC score')
plt.legend(bbox_to_anchor=(1,1), loc='upper left')
plt.savefig("SIN3A in 5 cell line.png")
plt.show()

# >50000 biosample.
df1 = pd.read_excel("prediction.xlsx", usecols=["biosample","AVG", "STDEV"],nrows=36, sheet_name="50000")

biosample_list, top_2000, top_4000, top_6000, top_8000, top_10000, top_20000, top_30000, top_40000, top_50000,  = [], [], [], [], [], [], [], [], [], []
sd_2000, sd_4000, sd_6000, sd_8000, sd_10000, sd_20000, sd_30000, sd_40000, sd_50000 = [], [], [], [], [], [], [], [], []
for i in range(len(df1["biosample"])):
    if len(biosample_list) == 0:
        biosample_list.append(df1["biosample"][i])
    elif biosample_list[-1] != df1["biosample"][i]:
        biosample_list.append(df1["biosample"][i])

    if i % 9 == 0:
        top_2000.append(df1["AVG"][i])
        sd_2000.append(df1["STDEV"][i])
    elif i % 9 == 1:
        top_4000.append(df1["AVG"][i])
        sd_4000.append(df1["STDEV"][i])
    elif i % 9 == 2:
        top_6000.append(df1["AVG"][i])
        sd_6000.append(df1["STDEV"][i])
    elif i % 9 == 3:
        top_8000.append(df1["AVG"][i])
        sd_8000.append(df1["STDEV"][i])
    elif i % 9 == 4:
        top_10000.append(df1["AVG"][i])
        sd_10000.append(df1["STDEV"][i])
    elif i % 9 == 5:
        top_20000.append(df1["AVG"][i])
        sd_20000.append(df1["STDEV"][i])
    elif i % 9 == 6:
        top_30000.append(df1["AVG"][i])
        sd_30000.append(df1["STDEV"][i])
    elif i % 9 == 7:
        top_40000.append(df1["AVG"][i])
        sd_40000.append(df1["STDEV"][i])
    else:
        top_50000.append(df1["AVG"][i])
        sd_50000.append(df1["STDEV"][i])

x = np.arange(len(biosample_list))
width = 0.1
plt.figure(figsize=(18,5))
plt.bar(x, top_2000, width, yerr=sd_2000, capsize=3, color='red', label='2000(7:1)')
plt.bar(x + width, top_4000, width, yerr=sd_4000, capsize=3, color='yellow', label='4000(15:1)')
plt.bar(x + 2*width,top_6000, width, yerr=sd_6000, capsize=3, color='orange', label='6000(23:1)')
plt.bar(x + 3*width,top_8000, width, yerr=sd_8000, capsize=3, color='green', label='8000(31:1)')
plt.bar(x + 4*width, top_10000, width, yerr=sd_10000, capsize=3, color='blue', label='10000(39:1)')
plt.bar(x + 5*width, top_20000, width, yerr=sd_20000, capsize=3, color='brown', label='20000(79:1)')
plt.bar(x + 6*width,top_30000, width, yerr=sd_30000, capsize=3, color='black', label='30000(119:1)')
plt.bar(x + 7*width,top_40000, width, yerr=sd_40000, capsize=3, color='pink', label='40000(159:1)')
plt.bar(x + 8*width,top_50000, width, yerr=sd_50000, capsize=3, color='olive', label='50000(199:1)')
plt.xticks(x + 8*width / 2, biosample_list)
plt.ylim(0.9,1)
plt.xlabel('Biosample')
plt.ylabel('AUC score')
plt.title('Average of 10 times AUC score')
plt.legend(bbox_to_anchor=(1,1), loc='upper left')

plt.savefig("4 biosamples.png")
plt.show()