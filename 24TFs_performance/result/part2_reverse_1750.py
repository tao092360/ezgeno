import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

df1 = pd.read_excel("prediction.xlsx", usecols=["TF", "Cell line","AVG", "STDEV"], sheet_name="reverse_1750")

TF_name, TF_list = dict(), []
for i in range(len(df1["TF"])):
    tf = df1["TF"][i]
    cell_line = df1["Cell line"][i]
    avg = df1["AVG"][i]
    sd = df1["STDEV"][i]

    if tf not in TF_name:
        TF_name[tf] = {'AVG':[], 'STDEV':[], 'cell line':[]}
        TF_list.append(tf)

    TF_name[tf]["cell line"].append(cell_line)
    TF_name[tf]["AVG"].append(avg)
    TF_name[tf]["STDEV"].append(sd)       

width = 0.2
x = np.arange(len(TF_name[TF_list[0]]["cell line"][:5]))
plt.figure(figsize=(15,5))

for i in range(len(TF_list)):
    TF = TF_list[i]
    print(type(TF_name[TF]['STDEV'][:5]))

    plt.subplot(int('12{}'.format(i+1)))
    plt.bar(x, TF_name[TF]['AVG'][:5], width, yerr=TF_name[TF]['STDEV'][:5], capsize=3, color='red', label='Top 1750')
    plt.bar(x+width, TF_name[TF]['AVG'][5:], width, yerr=TF_name[TF]['STDEV'][5:], capsize=3, color='blue', label='Last 1750')
    plt.xticks(x + width / 2, TF_name[TF_list[i]]["cell line"][:5])
    plt.xlabel(TF_list[i])
    if i==0:
        plt.ylabel('AUC score')
    plt.ylim(0.5,1)
    plt.title('Average of 10 times AUC score')

plt.legend(bbox_to_anchor=(1,1), loc='upper left')
plt.savefig("2 TFs in five cell lines.png")
plt.show()
