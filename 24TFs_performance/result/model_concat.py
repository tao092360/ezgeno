for i in range(40, 160):
    w = open('./model_structure/model_{}.csv'.format(i), 'w')
    w.write('ID,Layer_1,Layer_2,Layer_3\n')
    for j in range(1, 11):
        f = open('model_{}_{}.txt'.format(i, j), 'r')
        w.write('{},'.format(j))
        n = 1
        for line in f:

            layer = line.split('\n')[0]
            element = ''
            if 'dilation' in layer:
                element += 'dilated_conv_'
            elif 'conv' in layer:
                element += 'conv_'

            if '3' in layer:
                element += '3'
            elif '7' in layer:
                element += '7'
            elif '11' in layer:
                element += '11'
            elif '15' in layer:
                element += '15'
            elif '19' in layer:
                element += '19'

            if element != '':
                w.write(element)

                if n < 3:
                    w.write(',')
                n += 1

        w.write('\n')