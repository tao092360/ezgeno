import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Randomly selected 10 TFs in K562 cell line.
df = pd.read_excel("prediction.xlsx", usecols=["TF","AVG", "STDEV"],sheet_name="K562_10TF")

TF_name, top_500, top_1000, top_1500, top_2000 = [], [], [], [], []
sd_500, sd_1000, sd_1500, sd_2000 = [], [], [], []
for i in range(len(df["TF"])):
    if i < 10:
        TF_name.append(df["TF"][i])
        top_500.append(df["AVG"][i])
        sd_500.append(df["STDEV"][i])
        print(TF_name)
    elif i < 20:
        top_1000.append(df["AVG"][i])
        sd_1000.append(df["STDEV"][i])
    elif i < 30:
        top_1500.append(df["AVG"][i])
        sd_1500.append(df["STDEV"][i])
    elif i < 40:
        top_2000.append(df["AVG"][i])
        sd_2000.append(df["STDEV"][i])

x = np.arange(len(TF_name))
width = 0.2
plt.figure(figsize=(15,4))
plt.bar(x, top_500, width, yerr=sd_500, capsize=3, color='red', label='500(1:1)')
plt.bar(x+width, top_1000, width, yerr=sd_1000, capsize=3, color='green', label='1000(3:1)')
plt.bar(x + 2*width,top_1500, width, yerr=sd_1500, capsize=3, color='blue', label='1500(5:1)')
plt.bar(x + 3*width,top_2000, width, yerr=sd_2000, capsize=3, color='orange', label='2000(7:1)')
plt.xticks(x + 3*width / 2, TF_name)
plt.xlabel('TF')
plt.ylabel('AUC score')
plt.title('Average of 10 times AUC score')
plt.legend(bbox_to_anchor=(1,1), loc='upper left')
plt.savefig("randomly selected 10 TFs in K562.png")
plt.show()

# Randomly selected 2 TFs in five cell line.
df1 = pd.read_excel("prediction.xlsx", usecols=["TF", "Cell line","AVG", "STDEV"], sheet_name="different_cell_line")

TF_name, cell_line, top_500, top_1000, top_1500, top_2000 = [], [], [], [], [], []
sd_500, sd_1000, sd_1500, sd_2000 = [], [], [], []
for i in range(len(df1["TF"])):

    if i < 10:
        TF_name.append(df1["TF"][i])
        cell_line.append(df1["Cell line"][i])
        top_500.append(df1["AVG"][i])
        sd_500.append(df1["STDEV"][i])
    elif i < 20:
        top_1000.append(df1["AVG"][i])
        sd_1000.append(df1["STDEV"][i])
    elif i < 30:
        top_1500.append(df1["AVG"][i])
        sd_1500.append(df1["STDEV"][i])
    elif i < 40:
        top_2000.append(df1["AVG"][i])
        sd_2000.append(df1["STDEV"][i])

x = np.arange(len(cell_line[:5]))
width = 0.2
plt.figure(figsize=(15,5))
plt.subplot(121)
plt.bar(x, top_500[:5], width, yerr=sd_500[:5], capsize=3, color='red', label='500(1:1)')
plt.bar(x+width, top_1000[:5], width, yerr=sd_1000[:5], capsize=3, color='green', label='1000(3:1)')
plt.bar(x + 2*width,top_1500[:5], width, yerr=sd_1500[:5], capsize=3, color='blue', label='1500(5:1)')
plt.bar(x + 3*width,top_2000[:5], width, yerr=sd_2000[:5], capsize=3, color='orange', label='2000(7:1)')
plt.xticks(x + 3*width / 2, cell_line[:5])
plt.xlabel(TF_name[0])
plt.ylabel('AUC score')
plt.title('Average of 10 times AUC score')
#plt.legend(bbox_to_anchor=(1,1), loc='upper left')

plt.subplot(122)
x = np.arange(len(cell_line[5:]))
plt.bar(x, top_500[5:], width, yerr=sd_500[5:], capsize=3, color='red', label='500(1:1)')
plt.bar(x+width, top_1000[5:], width, yerr=sd_1000[5:], capsize=3, color='green', label='1000(3:1)')
plt.bar(x + 2*width,top_1500[5:], width, yerr=sd_1500[5:], capsize=3, color='blue', label='1500(5:1)')
plt.bar(x + 3*width,top_2000[5:], width, yerr=sd_2000[5:], capsize=3, color='orange', label='2000(7:1)')
plt.xticks(x + 3*width / 2, cell_line[5:])
plt.xlabel(TF_name[5])
#plt.ylabel('AUC score')
plt.title('Average of 10 times AUC score')
plt.legend(bbox_to_anchor=(1,1), loc='upper left')

plt.savefig("2 TFs in five cell lines.png")
plt.show()